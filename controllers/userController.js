const User = require("../models/user")
const auth = require("../auth")
const bcrypt = require("bcrypt")

// register user
module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10),
    })
    return newUser.save().then((saved, error) => {
        if(error){
            console.log(error)
            return false
        }else{
            return "User registered successfully."
        }
    })
}

// user login
module.exports.userLogin = (reqBody) => {
    return User.findOne({email:reqBody.email}).then(result => {
        if (result === null){
            return false
        }else{
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
            if(isPasswordCorrect){
                return {access: auth.createAccessToken(result.toObject())}
            }else{
                return false
            }
        }
    })
}

// user details
module.exports.getProfile = (data) => {
    return User.findById(data.userId).then(result => {
        if (result === null){
            return false
        }else{
            result.password = ""
            return result
        }
    })
}

// update user to admin
module.exports.updateUser = (reqParams, reqBody) => {
    let updatedUser = {
        isAdmin: reqBody.isAdmin
    }
    return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((result,error) => {
        if(error){
            return false
        }else{
            return true
        }
    })
}