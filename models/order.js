const mongoose = require("mongoose")
const orderSchema = new mongoose.Schema({
    totalAmount:{
        type: Number,
        required: [true, "Total Amount is required"]
    },
    purchasedOn:{
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model("Order", orderSchema)